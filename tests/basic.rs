#[cfg(test)]
mod tests {
    use serde_skip::*;
    use serde::{Deserialize, Serialize};
    use serde_json::json;

    #[derive(Debug, Serialize, Deserialize, PartialEq, Clone, Default)]
    struct TestNums {
        #[serde(skip_serializing_if = "is_default", default)]
        a: i8,
        #[serde(skip_serializing_if = "is_default", default)]
        b: i32,
        #[serde(skip_serializing_if = "is_default", default)]
        c: i128,
        #[serde(skip_serializing_if = "is_default", default)]
        d: f32,
        #[serde(skip_serializing_if = "is_default", default)]
        e: f64,
        #[serde(deserialize_with = "num_from_str_or_default", skip_serializing_if = "is_default")]
        f: u64,
        #[serde(deserialize_with = "num_from_str_or_default", skip_serializing_if = "is_default", default)]
        g: f64,
    }

    #[test]
    fn is_default_on_numbers() {
        let empty_struct = TestNums::default();
        assert_eq!("{}", &serde_json::to_string(&empty_struct).unwrap());

        let b_is_nine = {
            let mut t = TestNums::default();
            t.b = 9;
            t
        };
        assert_eq!("{\"b\":9}", &serde_json::to_string(&b_is_nine).unwrap());

        let e_is_pi = {
            let mut t = TestNums::default();
            t.e = std::f64::consts::PI;
            t
        };
        assert_eq!("{\"e\":3.141592653589793}", &serde_json::to_string(&e_is_pi).unwrap());
    }


    #[test]
    fn test_num_from_str_or_default() {
        let empty_struct = TestNums::default();
        assert_eq!("{}", &serde_json::to_string(&empty_struct).unwrap());

        let f_is_nine_v = json!({
            "f": "9"
        });
        let f_is_nine : TestNums = serde_json::from_value(f_is_nine_v).unwrap();
        assert_eq!("{\"f\":9}", &serde_json::to_string(&f_is_nine).unwrap());

        let g_is_pi = {
            let mut t = TestNums::default();
            t.g = std::f64::consts::PI;
            t
        };
        assert_eq!("{\"g\":3.141592653589793}", &serde_json::to_string(&g_is_pi).unwrap());
    }
}
