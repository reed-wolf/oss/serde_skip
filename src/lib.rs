use serde::{Deserialize, Deserializer};
use std::str::FromStr;
use std::fmt::Display;

pub fn is_default<T> (v: &T) -> bool
where T: PartialEq + Default  {
    T::default().eq(v)
}

/// Use as
/// ```ignore
/// #[serde(deserialize_with = "num_from_str_or_default")]
/// pub number: u64,
/// ```
pub fn num_from_str_or_default<'de, T, D>(deserializer: D) -> Result<T, D::Error>
where
    D: Deserializer<'de>,
    T: FromStr + serde::Deserialize<'de> + Default,
    <T as FromStr>::Err: Display,
{
    #[derive(Deserialize)]
    #[serde(untagged)]
    enum StringOrInt<T> {
        String(String),
        Number(T),
    }
    impl<T: Default> Default for StringOrInt<T> {
        fn default() -> Self { StringOrInt::Number(T::default()) }
    }

    match StringOrInt::<T>::deserialize(deserializer)? {
            StringOrInt::String(s) => s.parse::<T>().map_err(serde::de::Error::custom),
            StringOrInt::Number(i) => Ok(i),
    }
}
